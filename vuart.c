// SPDX-License-Identifier: GPL-2.0-or-later
/*
 * Copyright (C) 2019 CERN (www.cern.ch)
 *
 */


#include <linux/init.h>
#include <linux/ioport.h>
#include <linux/clk.h>
#include <linux/delay.h>
#include <linux/module.h>
#include <linux/tty.h>
#include <linux/tty_flip.h>
#include <linux/serial.h>
#include <linux/serial_core.h>
#include <linux/platform_device.h>
#include <linux/console.h>
#include <linux/device.h>
#include <linux/slab.h>

#include "include/hw/wb_uart.h"

struct wr_uart_port {
        struct uart_port        uart;
        struct clk              *clk;
        int                     may_wakeup;
        u32                     backup_imr;
        unsigned int            baud;
        bool                    tx_stopped;
};


static struct uart_driver wr_uart = {
        .owner          = THIS_MODULE,
        .driver_name    = "wr_uart",
        .dev_name       = "ttyWR",
        .major          = 1,
        .minor          = 0,
        .nr             = 1,
};

static inline struct wr_uart_port *to_wr_uart_port (struct uart_port *uart)
{
        return container_of(uart, struct wr_uart_port, uart);
}

static void wr_uart_console_putchar(struct uart_port *port, u8 ch)
{
        writel(ch, port->membase + UART_HOST_TDR_DATA_W(ch));

}
static void wr_uart_console_write(struct console *co, const char *s,
                                unsigned int count)
{
        struct uart_port *port = to_wr_uart_port(port);
        unsigned long flags;
        unsigned int  ctrl;
        struct mapping_desc *vuart;
        struct UART_WB *ptr;
        int sr = ptr->SR;

        ctrl = readl(port->membase + sr);

        /*tx_enable and disable bit*/
        writel(ctrl, port->membase + ptr->HOST_TDR);

        uart_console_write(port, s, count, wr_uart_console_putchar);
}

static int wr_uart_console_setup(struct console *co, char *options)
{
        struct uart_port *port = to_wr_uart_port(port);
        int baud = 9600;
        int bits = 8;
        int flow = 'n';
        int parity = 'n';

        if (!port->membase) {
                pr_debug("console on WR_TTY %i not present\n",
                         co->index);
                return -ENODEV;
        }

        if (options)
                uart_parse_options(options, &baud, &parity, &bits, &flow);

        return uart_set_options(port, co, baud, parity, bits, flow);
}

static struct console wr_console = {
        .name           = "ttyWR",
        .write          = wr_uart_console_write,
        .device         = uart_console_device,
        .setup          = wr_uart_console_setup,
        .flags          = CON_PRINTBUFFER,
        .index          = -1,
        .data           = &wr_uart,
};

#define WR_CONSOLE_DEVICE  (&wr_console)

static inline u32 wr_uart_readl (struct uart_port *port, u32 reg)
{
        return __raw_readl(port->membase + reg);
}

static inline void wr_uart_writel (struct uart_port *port, u32 reg, u32 value)
{
        __raw_writel(value, port->membase + reg);
}

static u_int wr_uart_tx_empty (struct uart_port *port)
{
        struct wr_uart_port *wr_uart_port = to_wr_uart_port(port);

        if (wr_uart_port->tx_stopped)
                return TIOCSER_TEMT;
        return 0;
}

/*
 * Start Tx
 */

static void wr_uart_tx_start (struct uart_port *port)
{
        struct wr_uart_port *wr_port = to_wr_uart_port(port);
        struct UART_WB *ptr;
        uint32_t val;

	ptr = kzalloc(sizeof(*ptr), GFP_KERNEL);
        if(!ptr)
                return -ENOMEM;

        wr_uart_writel(port, UART_SR_RX_RDY,ptr->SR );

        if (uart_circ_empty(&port->state->xmit))
                return;

}

static int wr_uart_tx_stop (struct uart_port *port)
{
        struct wr_uart_port *wr_port = to_wr_uart_port(port);
        
        struct UART_WB *ptr;

}

static void wr_uart_rx_start(struct uart_port *port)
{
        struct wr_uart_port *wr_port = to_wr_uart_port(port);
	struct UART_WB	*ptr;
	ptr = kzalloc(sizeof(*ptr), GFP_KERNEL);
        if(!ptr)
                return -ENOMEM;

	int rdr = ptr->HOST_RDR;
 
        if ( UART_HOST_RDR_RDY)
                wr_uart_readl(port, UART_HOST_RDR_DATA_R(rdr));
        return (rdr & UART_HOST_RDR_RDY) ? UART_HOST_RDR_DATA_R(rdr) : -1;

}

static int wr_uart_rx_stop(struct uart_port *port)
{
        /*check BSY status registers*/


}
static void wr_uart_release_port (struct uart_port *port)
{

        struct platform_device *mpdev = to_platform_device(port->dev->parent);
        int size = resource_size(mpdev->resource);

        release_mem_region(port->mapbase, size);

        if (port->flags & UPF_IOREMAP) {
                iounmap(port->membase);
                port->membase = NULL;
        }
}

static int wr_uart_request_port (struct uart_port *port)
{

        struct platform_device *mpdev = to_platform_device(port->dev->parent);
        int size = resource_size(mpdev->resource);

        if (port->flags & UPF_IOREMAP) {
                port->membase = ioremap(port->mapbase, size);
                if (port->membase == NULL) {
                        release_mem_region(port->mapbase, size);
                        return -ENOMEM;
                }
        }
        return 0;
}

static int wr_uart_startup(struct uart_port *port)
{

        struct platform_device *pdev = to_platform_device(port->dev);
        struct wr_uart_port *wr_port = to_wr_uart_port(port);
	struct UART_WB *ptr;
	ptr = kzalloc(sizeof(*ptr), GFP_KERNEL);
        if(!ptr)
                return -ENOMEM;

	wr_uart_readl(port, UART_SR_RX_RDY);

}


static int wr_uart_shutdown(struct uart_port *port)
{


}

static struct uart_ops wr_uart_ops = {
        .tx_empty       = wr_uart_tx_empty,
        .start_tx       = wr_uart_tx_start,
        .stop_tx        = wr_uart_tx_stop,
        .stop_rx        = wr_uart_rx_stop,
        .startup        = wr_uart_startup,
	.shutdown       = wr_uart_shutdown,
        .release_port   = wr_uart_release_port,
        .request_port   = wr_uart_request_port,
};

static void  wr_init_port(struct wr_uart_port *wr_port,
                                      struct platform_device *pdev)
{
        int ret;
        struct uart_port *port = &wr_port->uart;
        struct platform_device *mpdev = to_platform_device(pdev->dev.parent);

        port->iotype            = UPIO_MEM;
        port->flags             = UPF_BOOT_AUTOCONF | UPF_IOREMAP;
        port->ops               = &wr_uart_ops;
        port->fifosize          = 1;
        port->dev               = &pdev->dev;
        port->mapbase           = mpdev->resource[0].start;
      /***clock enable****/
        /*
        if (!wr_uart_port->clk) {
        wr_uart_port->clk = clk_get(&pdev->dev, "usart");
        clk_enable(wr_uart_port->clk);
        port->uartclk = clk_get_rate(wr_uart_port->clk);
        clk_disable(wr_uart_port->clk);
        */
}

static int wr_serial_probe(struct platform_device *pdev)
{       
        int ret;
        struct wr_uart_port *port = to_wr_uart_port(port);
        port = &pdev->id;
        port->backup_imr = 0;
        wr_init_port(port, pdev);
        uart_add_one_port(&wr_uart, &port->uart);
        platform_set_drvdata(pdev, port);
	
	ret = uart_register_driver(&wr_uart);
	printk(KERN_ALERT "uart driver %d",ret);
        return ret;
}

static int wr_serial_remove(struct platform_device *pdev)
{
        struct uart_port *port = platform_get_drvdata(pdev);
        platform_set_drvdata(pdev, NULL);
        uart_remove_one_port(&wr_uart, port);

        uart_unregister_driver(&wr_uart);
        return 0;
}

static struct platform_driver wr_serial_driver = {
 	.probe          = wr_serial_probe,
        .remove         = wr_serial_remove,
        .driver         = {
                .name   = "wr_serial",
	},
};

module_platform_driver(wr_serial_driver);

MODULE_DESCRIPTION("vuart driver");
MODULE_LICENSE("GPL v2");

