// SPDX-License-Identifier: GPL-2.0-or-later
// /*
//  * * Copyright (C) 2019 CERN (www.cern.ch)
//   * */

#include <linux/init.h>
#include <linux/module.h>
#include <linux/pci.h>
#include <linux/pci_ids.h>
#include <linux/sched.h>
#include <linux/tty.h>
#include <linux/serial.h>
#include <linux/tty_flip.h>
#include <linux/slab.h>
#include <linux/io.h>
#include <linux/poll.h>
#include <linux/wait.h>
#include <linux/timer.h>
#include <linux/semaphore.h>

#include "wb_uart.h"

#define VUART_TTY_MAJOR  240
#define VUART_TTY_MINOR  1
#define DELAY_TIME       HZ * 2	
#define VUART_TTY_CHARACTER   'v'
#define UART_BASE_ADDRESS 0x20500
struct pci_dev *pdev;
static resource_size_t bar_start;
static resource_size_t bar_addr;

static struct pci_device_id pcie_ids[] = {
	{ PCI_DEVICE(0x10ee, 0x7022) },
	{0,},	
};

MODULE_DEVICE_TABLE(pci, pcie_ids);

static int spec7_pci_init(struct pci_dev *pcidev, struct pci_device_id *id)
{

	int ret = 0;
        u16 vendor_id;
        u16 device_id;
	static resource_size_t bar_len;	
	ret = pci_enable_device(pcidev);
        if (ret) {
		printk(KERN_ALERT "Device not enabled\n");
	 }

        pci_read_config_word(pcidev, 0, &vendor_id);
        pci_read_config_word(pcidev, 2, &device_id);
        printk(KERN_ALERT "PCIe driver detected device %x %x", vendor_id, device_id);

	/* io enable and address check at BAR0*/
	bar_start = pci_resource_start(pcidev, 1);
	printk(KERN_ALERT "PCIe driver detected device at address %llx ",bar_start);
	
	bar_len = pci_resource_len(pcidev, 1);
	bar_addr = (resource_size_t)pci_iomap(pcidev, 1 , bar_len);

	return 0;

}

/*****************************TTY declarations************************/

struct vuart_serial {
	struct tty_struct	*tty;		/* pointer to the tty for this device */
	int			open_count;	/* number of times this port has been opened */
	struct mutex		mutex;		/* locks this structure */
	struct timer_list	timer;
};

static struct vuart_serial *vuart_table[VUART_TTY_MINOR];	/* initially all NULL */
static struct tty_port vuart_tty_port[VUART_TTY_MINOR];

static void vuart_timer(struct timer_list *t)
{
	struct vuart_serial *vuarts = from_timer(vuarts, t, timer);
	struct tty_struct *tty;
	struct tty_port *port;
	int i;
	char data[1] = {VUART_TTY_CHARACTER};
	int data_size = 1;

	if (!vuarts)
		return;

	tty = vuarts->tty;
	port = tty->port;

	
	for (i = 0; i < data_size; ++i) {
		if (!tty_buffer_request_room(port, 1))
			tty_flip_buffer_push(port);
		tty_insert_flip_char(port, data[i], TTY_NORMAL);
	}
	tty_flip_buffer_push(port);

	
	vuarts->timer.expires = jiffies + DELAY_TIME;
	add_timer(&vuarts->timer);
}


static int vuart_open(struct tty_struct *tty, struct file *file)
{
	struct vuart_serial *vuarts;
	int index;

	/* initialize the pointer in case something fails */
	tty->driver_data = NULL;

	/* get the serial object associated with this tty pointer */
	index = tty->index;
	vuarts = vuart_table[index];
	if (vuarts == NULL) {
		/* first time accessing this device, let's create it */
		vuarts = kmalloc(sizeof(*vuarts), GFP_KERNEL);
		if (!vuarts)
			return -ENOMEM;

		mutex_init(&vuarts->mutex);
		vuarts->open_count = 0;

		vuart_table[index] = vuarts;
	}

	mutex_lock(&vuarts->mutex);

	/* save our structure within the tty structure */
	tty->driver_data = vuarts;
	vuarts->tty = tty;

	++vuarts->open_count;
	if (vuarts->open_count == 1) {
		/* this is the first time this port is opened */
		/* do any hardware initialization needed here */

		/* create our timer and submit it */
		timer_setup(&vuarts->timer, vuart_timer, 0);
		vuarts->timer.expires = jiffies + DELAY_TIME;
		add_timer(&vuarts->timer);
	}

	mutex_unlock(&vuarts->mutex);
	return 0;
}

static void do_close(struct vuart_serial *vuarts)
{
	mutex_lock(&vuarts->mutex);

	if (!vuarts->open_count) {
		/* port was never opened */
		goto exit;
	}

	--vuarts->open_count;
	if (vuarts->open_count <= 0) {
		/* The port is being closed by the last user. */
		/* Do any hardware specific stuff here */

		/* shut down our timer */
		del_timer(&vuarts->timer);
	}
exit:
	mutex_unlock(&vuarts->mutex);
}

static void vuart_close(struct tty_struct *tty, struct file *file)
{
	struct vuart_serial *vuarts = tty->driver_data;

	if (vuarts)
		do_close(vuarts);
}

static int vuart_write(struct tty_struct *tty,
		      const unsigned char *buffer, int count)
{
	struct vuart_serial *vuarts = tty->driver_data;
	int i;
	int ret = -EINVAL;

	if (!vuarts)
		return -ENODEV;

	mutex_lock(&vuarts->mutex);

	if (!vuarts->open_count)
		/* port was not opened */
		goto exit;

	pr_debug("%s - ", __func__);
	for (i = 0; i < count; ++i)
		pr_info("%02x ", buffer[i]);
	pr_info("\n");

exit:
	mutex_unlock(&vuarts->mutex);
	return ret;
}


static int vuart_write_room(struct tty_struct *tty)
{
	struct vuart_serial *vuarts = tty->driver_data;
	int room = -EINVAL;

	if (!vuarts)
		return -ENODEV;

	mutex_lock(&vuarts->mutex);

	if (!vuarts->open_count) {
		/* port was not opened */
		goto exit;
	}

	/* calculate how much room is left in the device */
	room = 255;

exit:
	mutex_unlock(&vuarts->mutex);
	return room;
}

static void vuart_set_termios(struct tty_struct *tty, struct ktermios *old_termios )
{
	unsigned int baud = 0;
	baud = tty_get_baud_rate(tty);
	tty_termios_encode_baud_rate(&tty->termios, baud, baud);
}

static const struct tty_operations vuart_ops = {
	.open = vuart_open,
	.close = vuart_close,
	.write = vuart_write,
	.write_room = vuart_write_room,
	.set_termios = vuart_set_termios,
	
	/* @optional functions
 	.proc_show = vuart_proc_show,
	.tiocmget = vuart_tiocmget,
	.tiocmset = vuart_tiocmset,
	.ioctl = vuart_ioctl,
	*/
};

static struct tty_driver *vuart_tty_driver;

static int __init vuart_tty_init(void)
{
	int ret = 0;
	//spec7_pci_init(pdev, pcie_ids);

	vuart_tty_driver= alloc_tty_driver(VUART_TTY_MINOR);
	if (!vuart_tty_driver)
		return -ENOMEM;

	vuart_tty_driver->owner = THIS_MODULE;
	vuart_tty_driver->driver_name = "vuart_tty";
	vuart_tty_driver->name = "vtty";
	vuart_tty_driver->major = VUART_TTY_MAJOR,
	vuart_tty_driver->type = TTY_DRIVER_TYPE_SERIAL,
	vuart_tty_driver->subtype = SERIAL_TYPE_NORMAL,
	vuart_tty_driver->flags = TTY_DRIVER_REAL_RAW | TTY_DRIVER_DYNAMIC_DEV,
	vuart_tty_driver->init_termios = tty_std_termios;
	vuart_tty_driver->init_termios.c_cflag = B9600 | CS8 | CREAD | HUPCL | CLOCAL;
	tty_set_operations(vuart_tty_driver, &vuart_ops);	
	
	ret = tty_register_driver(vuart_tty_driver);
	if (ret) {
		pr_err("Failed to register VUART tty driver");
		put_tty_driver(vuart_tty_driver);
		return ret;
	}

	tty_register_device(vuart_tty_driver, VUART_TTY_MINOR, NULL);

	return 0;	
}


static void __exit vuart_tty_exit(void)
{
	struct vuart_serial *vuarts;
	tty_unregister_device(vuart_tty_driver, VUART_TTY_MINOR);
	tty_unregister_driver(vuart_tty_driver);
	vuarts = vuart_table[VUART_TTY_MINOR];
	if (vuarts) {
		while (vuarts->open_count)
		{
			do_close(vuarts);
			//del_timer(vuarts->timer);
			//kfree(vuarts->timer);
			kfree(vuarts);
			vuart_table[VUART_TTY_MINOR] = NULL;
		}
	}
}

module_init(vuart_tty_init);
module_exit(vuart_tty_exit);
MODULE_LICENSE("GPL");
